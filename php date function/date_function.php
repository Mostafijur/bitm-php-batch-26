<?php

/* 
 * date() function it has two parameter format and timestamp
   date(format,timestamp)
 */

echo date("Y/m/d")."<br>";

echo date("Y,m,d")."<br>";

echo date("Y-m-d")."<br>";

echo date("l");

/*2016/05/30
  2016,05,30
  2016-05-30
  Monday */

?>
