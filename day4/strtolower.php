<?php

/* 
 * the strtolower() function does the exact opposite of strtoupper()
 *  and converts a string into all lowercase letters
 */

$str="MY NAME IS DR. MD. MOSTAFIJUR RAHMAN";

$lower=  strtolower($str);
echo $lower;

//my name is dr. md. mostafijur rahman
?>