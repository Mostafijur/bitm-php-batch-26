<?php

/* 
 * The substr_replace() function replaces a part of a string with another string.
 * Replace "Hello" with "world":
 *Syntax
  substr_replace(string,replacement,start,length) 
 *  	Returns the replaced string. If the string is an array then the array is returned
 */

echo substr_replace("Hello world", "Bangladesh",6);
//Hello Bangladesh
?>

<hr>

<?php
 $str="I am Hasan";
 //Start replacing at the 6th position in the string (replace "world" with "earth"):
 echo substr_replace($str,"Mostafijur", 5);
// I am Mostafijur

echo '<br>';
//Start replacing at the 5th position from the end of the string (replace "world" with "earth"):
echo substr_replace($str,"Kasem Ali", -5);
?>

<hr>

<?php
$str="Mostafijur";
//Insert "Hello" at the beginning of "world":
echo substr_replace($str,"Hello ",0,0);
 //Hello Mostafijur 

?>

<hr>

<?php
$replace=array("1: AAA","2: AAA","3: AAA");
//Replace multiple strings at once. Replace "AAA" in each string with "BBB":
echo implode("<br>", substr_replace($replace, "BBB",3,3));
//1: BBB
//2: BBB
//3: BBB

?>