<?php

/* 
 * Returns an array of strings
 * The explode() function breaks a string into an array
 * Syntax
   explode(separator,string,limit)
 * explode(' ',variable name,2) 
 */

$str = "Hello world. It's a beautiful day.";
echo '<pre>';
print_r(explode(" ", $str));
echo '</pre>';
//Array ( [0] => Hello [1] => world. [2] => It's [3] => a [4] => beautiful [5] => day. ) 

?>
hghgh

<hr>
<?php

$str = "Hello world. It's a beautiful day.";
//print_r(explode(" ", $str));
echo '<pre>';
var_dump(explode(" ",$str));
echo '</pre>';



?>


<hr>
<?php

$str="peace1 peace2 peace3 peace4";
$pieces=  explode(" ",$str);
echo $pieces[0];
echo '<br>';
echo $pieces[2];



?>

<hr>
<?php

$str="peace1, peace2, peace3, peace4";
//zero limit
echo '<pre>';
print_r(explode(",",$str,0));
echo '</pre>';
//positive limit
echo '<pre>';
print_r(explode(",",$str,3));
echo '</pre>';
//negetive limit
echo '<pre>';
print_r(explode(",",$str,-2));
echo '</pre>';
?>