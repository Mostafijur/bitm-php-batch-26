<?php

/* 
 * Trim() is dedicated to remove white spaces and predefined characters from a both the sides of a string.
 * Remove characters from both sides of a string
 * Syntax
  trim(string,charlist) 
 */

$str="Hello world";
echo $str ."<br>";

echo trim($str,"Held");
//o wor 
?>

<hr>

<?php
$str = " Hello World! ";
echo "Without trim: " . $str;
echo "<br>";
echo "With trim: " . trim($str);
?>