<?php

/* 
 * Join array elements with a string:
 * Join array elements with a glue string. 
 * The implode() function returns a string from the elements of an array.
 * Syntax
   implode(separator,array) 
 * string implode ( string $glue , array $pieces )
 */

$str=array('Hello','World','Beautiful','Day');
echo implode(' ',$str);

//Hello World Beautiful Day 
?>

<hr>

<?php

$str=array('Name','Email','Phone');
$data=  implode(',', $str);//implode($glue=>' ',variable-name=>$str);
echo $data;

//Name,Email,Phone 

?>

<hr>


<?php
$picnames = array("pic1.jpg", "pic2.jpg", "pic3.jpg", "pic4.jpg", "pic5.jpg", "pic6.jpg", "pic7.jpg");
$allpics = implode("--->", array_slice($picnames, 0, 5)); 
echo $allpics;

// pic1.jpg--->pic2.jpg--->pic3.jpg--->pic4.jpg--->pic5.jpg
?>