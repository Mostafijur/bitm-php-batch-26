<?php

/* 
 *Very often when working with strings, you’ll want to know how long it is. 
 * For example, when dealing with a form, you may have a field where you want to make sure users can’t go over a certain number of characters.
 *  To count the number of characters in a string, you can use the strlen() function:
 * 
 * PHP has a predefined function to get the length of a string. Strlen() displays the length of any string. 
 * It is more commonly used in validating input fields where
 *  the user is limited to enter a fixed length of characters.
 * Syntax

    Strlen(string);
 */
//Getting length of a String

$str="How are you?";
$len=  strlen($str);
echo $len;
// Displays:12

?>
<hr>



<?php

$str="How long is a peace of string";
$size= strlen($str);

 if($size<=30){
   echo "Pleace see ur lenth =".$size; 
}  else {
   echo "No match";
}

// Displays:29
?>

<hr>
<?php

$str="How long is a peace of string";
//$size= strlen($str);

 if(strlen($str)<=30){
   echo "Your lenth is right position "; 
}  else {
   echo "No match";
}

// Displays:29
?>

