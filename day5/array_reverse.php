<?php

/* 
 * The array_reverse() function returns an array in the reverse order.
 * array_reverse(array,preserve)
 */

$cars=array("a"=>"BMW","b"=>"Toyota","c"=>"Volvo");
echo '<pre>';
print_r(array_reverse($cars));
echo '<pre>';


/*Array
(
    [c] => Volvo
    [b] => Toyota
    [a] => BMW
)*/
?>

<hr>

<?php
//Return the original array, the reversed array and the preserved array
$a=array("Volvo","XC90",array("BMW","Toyota"));
$reverse=array_reverse($a);
$preserve=array_reverse($a,true);
echo '<pre>';
print_r($a);
echo '</pre>';
echo '<pre>';
print_r($reverse);
echo '</pre>';
print_r($preserve);

/*Array
(
    [0] => Volvo
    [1] => XC90
    [2] => Array
        (
            [0] => BMW
            [1] => Toyota
        )

)

Array
(
    [0] => Array
        (
            [0] => BMW
            [1] => Toyota
        )

    [1] => XC90
    [2] => Volvo
)

Array
(
    [2] => Array
        (
            [0] => BMW
            [1] => Toyota
        )

    [1] => XC90
    [0] => Volvo
)*/
?> 