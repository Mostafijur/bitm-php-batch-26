<?php

/* 
 * The array_unshift() function inserts new elements to an array. 
 * The new array values will be inserted in the beginning of the array
 * array_unshift(array,value1,value2,value3...) 
 */

$a=array("a"=>"red","b"=>"green");
array_unshift($a,'white');
echo '<pre>';
print_r($a);
echo '</pre>';

/*Array ( [0] => white [a] => red [b] => green ) */
?>


<hr>
<?php


$authors = array( "Java", "PHP", "CSS", "HTML" ); 
$newAuthors = array( "Javascript", "Python" ); 
echo array_unshift($authors, $newAuthors )."<br/>";

echo '<pre>';
print_r( $authors ); 

/*Array
(
    [0] => Array
        (
            [0] => Javascript
            [1] => Python
        )

    [1] => Java
    [2] => PHP
    [3] => CSS
    [4] => HTML
)*/
?>