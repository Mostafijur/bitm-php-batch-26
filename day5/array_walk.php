<?php

/* 
 *The array_walk() function runs a user-defined function on each array element.
 * The array's keys and values are parameters in the function.
 * We can change the value in the user-defined function by specifying the first parameter as a reference: &$value.
 * array_walk(array,myfunction,parameter...)
 */
//Map an array with user defined function
function myFunction($value,$key){
    echo "The  key  $key has the value  $value"."<br>";
    
}
$a=array("a"=>"A","b"=>"B","c"=>"C");
array_walk($a,"myFunction");

/*The key a has the value A
 The key b has the value B
 The key c has the value C*/
?>

<hr>

<?php
//With a parameter:
function mytest($value,$key,$p){
       echo "$key $p $value"."<br>";
    }
    $a=array("a"=>"A","b"=>"B","c"=>"java2s.com");
    array_walk($a,"mytest","has the value");
    
    /*a has the value A
    b has the value B
    c has the value java2s.com*/

?>

<hr>

<?php
function changevalue(&$value,$key){
    $value="added"; 
    
}
$a=array("a"=>"A","b"=>"B","c"=>"PHP");
array_walk($a,"changevalue");
echo '<pre>';
print_r($a);

/*Array
(
    [a] => added
    [b] => added
    [c] => added
)*/

?>