<?php

/* 
 * The array_values() function returns an array containing all the values of an array.

 * Tip: The returned array will have numeric keys, starting at 0 and increase by 1
 * array_values(array) 

 */

$people=array("name"=>"Mohammad(S)","age"=>65,"country"=>"Macca");
print_r($people);
//Array ( [name] => Mohammad(S) [age] => 65 [country] => Macca ) 

$values=  array_values($people);
echo '<pre>';
print_r($values);


//Return all the values of an array (not the keys)
/*Array
(
    [0] => Mohammad(S)
    [1] => 65
    [2] => Macca
)*/

?>