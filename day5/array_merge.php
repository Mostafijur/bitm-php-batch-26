<?php

/* 
 *The array_merge() function merges one or more arrays into one array.
 * Syntax
    array_merge(array1,array2,array3...) 
 */

$a=array('A','B','C','D');
$b=array('E','F','G','H');
//Merge two arrays into one array:
$merges=array_merge($a,$b);
echo '<pre>';
print_r($merges);
echo '</pre>';

/*Array
(
    [0] => A
    [1] => B
    [2] => C
    [3] => D
    [4] => E
    [5] => F
    [6] => G
    [7] => H
)*/
?>

<hr>

<?php
/*An element with the same numeric key doesn't get overwritten;
 *  instead the new element is added to the end of the array and given a new index: */

  $authors = array( "Java", "PHP", "CSS", "HTML" ); 
  $merges=array_merge($authors,array('0'=>'C#'));
  echo '<pre>';
  print_r($merges);
  echo '</pre>';

//Array ( [0] => Java [1] => PHP [2] => CSS [3] => HTML [4] => C# ) 

?>


<hr>

<?php
//Merge two associative arrays into one array

 $a=array('a'=>'A','b'=>'B','c'=>'C');
 $b=array('d'=>'D','e'=>'E','f'=>'F');
 echo '<pre>';
 print_r(array_merge($a,$b));
 echo '</pre>';

/*Array
(
    [a] => A
    [b] => B
    [c] => C
    [d] => D
    [e] => E
    [f] => F
)*/

?>