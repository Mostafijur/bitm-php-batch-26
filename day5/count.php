<?php

/* 
 * The count() function returns the number of elements in an array.
 * count(array,mode);
 */

$car=array("A","B","C","BMW");
echo count($car);
//4
?>


<hr>

<?php

$a=array("volvo"=>array("v10","v23"),"BMW"=>array("xc1","xc2"),"toyota"=>array("tox3","tox5"));

echo 'Normal count : '.count($a)."<br>";
echo 'Recursive count : ' .  count($a,1);
//Normal count : 3
//Recursive count : 9

?>