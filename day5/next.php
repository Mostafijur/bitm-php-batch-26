<?php

/* 
 * The next() function moves the internal pointer to, and outputs, the next element in the array.
 * next(array)
 */

$colors=array("red","green","pink","blue");

echo current($colors)."<br>";

echo next($colors);
//Output the value of the current and the next element in the array

/*red
green*/
?>