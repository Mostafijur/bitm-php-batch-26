<?php

/* 
 * The array_pad() function inserts value to an array element.
 * Syntax
    array_pad(array,size,value) 
 */


//Return 5 elements and insert a value of "Green" to the new elements in the array:
$colors=array('Red','Blue');
echo '<pre>';
print_r(array_pad($colors,5,'Green'));
echo '</pre>';

/*Array
(
    [0] => Red
    [1] => Blue
    [2] => Green
    [3] => Green
    [4] => Green
)*/
?>


<hr>

<?php
 
//Using a negative size parameter

$colors=array('Red','Blue');
echo '<pre>';
print_r(array_pad($colors,-5,'Green'));
echo '</pre>';

/*Array
(
    [0] => Green
    [1] => Green
    [2] => Green
    [3] => Red
    [4] => Blue
)*/


?>