<?php

/* 
 * The array_shift() function removes the first element from an array, and returns the value of the removed element.
 * array_shift(array) 
 */
//Remove the first element (red) from an array, and return the value of the removed element
$a=array("a"=>"red","b"=>"green","c"=>"blue");
echo array_shift($a);
//red

?>


<hr>

<?php
$a=array("a"=>"Web app develoed PHP","b"=>"Java","c"=>"Wordpress");
echo array_shift($a);
echo '<pre>';
var_dump($a);

//Web app develoed PHP
/*array(2) {
  ["b"]=>
  string(4) "Java"
  ["c"]=>
  string(9) "Wordpress"
}*/
?>