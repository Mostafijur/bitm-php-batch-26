<?php

/* 
 * The compact() function creates an array from variables and their values.
 * compact(var1,var2...) 
 */

//Create an array with compact
$firstname = "James";
$lastname = "Smith";
$age = "23";

$result = compact("firstname", "lastname", "age");
echo '<pre>';
print_r($result);
echo '</pre>';

/*Array
(
    [firstname] => James
    [lastname] => Smith
    [age] => 23
)*/
?>

<hr>
<?php
// Using a string that does not match a variable, and an array of variable names
$firstname = "James";
$lastname = "Smith";
$age = "23";

$name = array("firstname", "lastname");
$result = compact($name, "location", "age");
echo '<pre>';
print_r($result);


/*Array
(
    [firstname] => James
    [lastname] => Smith
    [age] => 23
)*/
?>