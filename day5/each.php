<?php

/* 
 * The each() function returns the current element key and value, and moves the internal pointer forward.
  
  This element key and value is returned in an array with four elements. 
 * Two elements (1 and Value) for the element value, and two elements (0 and Key) for the element key. 
 * each(array) 
 */

$people=array("A","B","C","D");
echo '<pre>';
print_r(each($people));
echo '<pre>';
/*Array
(
    [1] => A
    [value] => A
    [0] => 0
    [key] => 0
)*/

?>

<hr>


<?php
/*A loop to output the whole array:*/
$people = array("A", "B", "C", "D");

reset($people);

while (list($key, $val) = each($people)){
  echo "$key => $val";
}
/*0 => A1 => B2 => C3 => D*/
?>


<hr>

<?php
/*We can use an index of either 0 or "key" to access the current element's key,
  or an index of 1 or "value" to access its value. For example:*/
$myBook=array("title"=>"Learn PHP",
              "author"=>"Hasin Hyder",
              "published"=>2000);


$element=  each($myBook);

echo "key: ".$element[0]."<br>";
echo "value: ".$element[1]."<br>";
echo "key: ".$element['key']."<br>";
echo "value: ".$element['value']."<br>"; 
/*
key: title
value: Learn PHP
key: title
value: Learn PHP*/

?>


<hr>

<?php

$people=array("A","B","C","D");

echo current($people)."<br>"; // The current element is A
echo next($people)."<br>";// The next element of A is B
echo current($people)."<br>";// Now the current element is B
echo prev($people)."<br>";// The previous element of B is A
echo end($people)."<br>";// The last element is D
echo prev($people)."<br>";// The previous element of D is C
echo current($people)."<br>";// Now the current element is C
echo reset($people)."<br>";// Moves the internal pointer to the first element of the array, which is A
echo next($people)."<br>";// The next element of A is B

print_r(each($people));// Returns the key and value of the current element (now B), and moves the internal pointer forwar

/* 
A
B
B
A
D
C
C
A
B
Array
(
    [1] => B
    [value] => B
    [0] => 1
    [key] => 1
)
*/

?>