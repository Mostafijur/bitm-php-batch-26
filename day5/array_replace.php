<?php

/* 
 *The array_replace() function replaces the values of the first array with the values from following arrays.
 * array_replace(array1,array2,array3...)
 */

$name=array("Hasan","Ali");
$new_name=array("Mostafijur","Rahman");
echo '<pre>';
print_r(array_replace($name,$new_name));
//Replace the values of the first array ($a1) with the values from the second array ($a2)
//Array ( [0] => Mostafijur [1] => Rahman ) 
echo '</pre>';
echo '<pre>';
var_dump(array_replace($name,$new_name));
echo '</pre>';
?>

<hr>

<?php

$a=array("p"=>"PYTHON","j"=>"JAVA");
$b=array("p"=>"PHP");

echo '<pre>';
print_r(array_replace($a,$b));
echo '</pre>';

/*Array
(
    [p] => PHP
    [j] => JAVA
)*/

?>

<hr>

<?php
//If a key exists in array2 and not in array1:
$a=array("a"=>"orange","green");
$b=array("a"=>"red","b"=>"white");
        
echo '<pre>';
print_r(array_replace($a,$b));
echo '</pre>';
 /*Array
(
    [a] => red
    [0] => green
    [b] => white
)*/
?>


<hr>

<?php
//Using three arrays - the last array ($a3) will overwrite the previous ones ($a1 and $a2)
$a=array("A","B");
$b=array("A","Mostafijur");
$c=array("B","C");
echo '<pre>';
print_r(array_replace($a,$b,$c));
echo '</pre>';
/*Array
(
    [0] => B
    [1] => C
)*/
 

?>


<hr>

<?php
//Using numeric keys - If a key exists in array2 and not in array1
$a=array("A","B","C","D");
$b=array(0=>"Mostafijur",30=>"Rahman");
echo '<pre>';
print_r(array_replace($a,$b));
echo '</pre>';
/*Array
(
    [0] => Mostafijur
    [1] => B
    [2] => C
    [3] => D
    [30] => Rahman
)*/

?>