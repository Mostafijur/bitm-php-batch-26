<?php

/* 
 * The shuffle() function randomizes the position of the array elements.
 * shuffle(array) 
 */

$value = array("A", "B", "C", "java2s.com");
shuffle($value);
echo '<pre>';
print_r($value);

/*Array
(
    [0] => A
    [1] => C
    [2] => B
    [3] => java2s.com
)*/

?>

<hr>

<?php

//Randomize the order of the elements in the array:

$my_array = array("red","green","blue","yellow","purple");

shuffle($my_array);

print_r($my_array);

/*Array
(
    [0] => purple
    [1] => green
    [2] => blue
    [3] => red
    [4] => yellow
)*/

?>