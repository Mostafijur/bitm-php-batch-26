<?php

/* 
 * The array_rand() function returns a random key from an array
 * Syntax
   array_rand(array,number) 
 */

$a=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");

print_r(array_rand($a,1))."<br/>";
echo '<pre>';
print_r(array_rand($a,2));
echo '</pre>';
?>