<?php

/* 
 * The array_key_exists() function checks an array for a specified key,
   and returns true if the key exists and false if the key does not exist.
 * Syntax
   array_key_exists(key,array) 
 */

$a=array('volvo'=>'PHP','bmw'=>'java','tyota'=>'Python');

if(array_key_exists('volvo', $a)){
    echo 'Key exists';
}  else {
    echo "key doesn't exists! ";
    //Key exists
}
?>

<hr>


<?php

$a=array('Volvo','BMW','Tyota');
if(array_key_exists('0', $a)){
    echo 'Key exists';
    
}  else {
    echo 'Key does not exists!';
}

//Key exists
?>