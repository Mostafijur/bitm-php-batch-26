<?php

/* 
 * The in_array() function returns true if an array contains a specific value; otherwise, it will return false: 
 */

$people=array("Mostafij","Habib","Rana","Muaz",20);

if(in_array(20,$people)){
    
    echo "Match found<br>";
    
}  else {
    echo "Match not found<br>";
}

if(in_array("Nahid",$people)){
    echo "Match found<br>";
}  else {
    echo "Match not found<br>";
}

//Match found
//Match not found

?>

<hr>


<?php
//Is an element in an array
$needles="Muaz";
$haystack=array("Mostafijur","Rana","Muaz");
if(in_array($needles,$haystack)){
    
     echo "Match found<br>";
}  else {
    
     echo "Match not found<br>";
}
 //Match found

?>

<hr>

<?php
//Is an element in an array
$needle=5;
$name="Rana";
$haystack=array("Mostafijur",5,"Muaz");
if(in_array($needles,$haystack,true)){
    
     echo  "$needle is in the array <br>";
}  else {
    
     echo  "$needle is not in the array n <br>";
}

if(in_array($name,$haystack,TRUE)){ 
    
 echo  "$name is in the array <br>";
}  else {
    
   echo  "$name is not in the array <br>";
}

 

?>
