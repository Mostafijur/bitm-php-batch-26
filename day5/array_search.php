<?php

/* 
 * The array_search() function search an array for a value and returns the key.
 * array_search(value,array,strict)
 */
//Search an array for the value "PHP" and return its key
$a=array("a"=>"Java","b"=>"PHP","c"=>"Python");

echo array_search("PHP",$a);
//key b
?>

<hr>

<?php
$a=array("a"=>"5","b"=>"5","c"=>5);

echo array_search(5,$a,true);

//c
?>