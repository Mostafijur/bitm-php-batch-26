<?php

/* 
 * The array_unique() function removes duplicate values from an array. 
 * If two or more array values are the same, the first appearance will be kept and the other will be removed.
 * array_unique(array) 
 */

//Returns the unique values with duplicate values removed

$a=array("A","B","C","B","D","E","D");
echo '<pre>';
print_r(array_unique($a));

/*Array
(
    [0] => A
    [1] => B
    [2] => C
    [4] => D
    [5] => E
)*/
?>