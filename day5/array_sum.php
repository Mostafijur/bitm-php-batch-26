<?php

/* 
 *The array_sum() function returns the sum of all the values in the array.
 * array_sum(array)
 */

//Sum an indexed array

$number=array(5,10,15);
echo array_sum($number);

//30 
?>


<hr>

<?php
//Return the sum of all the values in the array (2.2+1.7+0.9)

$a=array("a"=>2.2,"b"=>1.7,"c"=>0.9);
echo array_sum($a);

//4.8

?>