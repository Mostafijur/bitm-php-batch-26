<?php

/* 
 * The asort() function sorts an associative array in ascending order, according to the value.
 * asort(array,sortingtype);
 */
//Sort an associative array in ascending order, according to the value
$age=array("Hasan"=>10,"Muaz"=>20,"Arif"=>30);
asort($age);
print_r($age);
/*Array ( [Hasan] => 10 [Muaz] => 20 [Arif] => 30 ) */
?>

<hr>

<?php

//asort() works by directly changing the value you pass in.
// The return value is either true or false, depending on whether the sorting was successful.
$capitalcities['key3']='Z';
$capitalcities['key1']='Y';
$capitalcities['key2']='X';
asort($capitalcities);
echo '<pre>';
print_r($capitalcities);

/*Array
(
    [key2] => X
    [key1] => Y
    [key3] => Z
)*/

?>