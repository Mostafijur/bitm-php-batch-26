<?php

/* 
 * The array_pop() function deletes the last element of an array.
 * Syntax
   array_pop(array) 
 */

$a=array('PHP','JAVA','HTML','CSS','PYTHON');
$delete=array_pop($a);
echo '<pre>';
print_r($a);

/*Array
(
    [0] => PHP
    [1] => JAVA
    [2] => HTML
    [3] => CSS
)*/
echo '</pre>'; 
print_r($delete);

//PYTHON

?>

<hr>


<?PHP
$myBook = array( "title" =>  "Learn PHP from java2s.com", 
                 "author" =>  "java2s.com", 
                 "pubYear" =>  2000 ); 

echo array_pop( $myBook ) ;
//2000
echo '<pre>';
print_r( $myBook );
echo '</pre>';

/*Array
(
    [title] => Learn PHP from java2s.com
    [author] => java2s.com
)*/
?>