<?php

/* 
 * The array_replace_recursive() function replaces the values of the first array with the values from following arrays recursively.
 * array_replace_recursive(array1,array2,array3...)
 */

$a=array("a"=>array("A"),"b"=>array("Java","PHP"));
$b=array("a"=>array("B"),"b"=>array("HTML"));
echo '<pre>';

print_r(array_replace_recursive($a,$b));

echo '</pre>';
/*Array
(
    [a] => Array
        (
            [0] => B
        )

    [b] => Array
        (
            [0] => HTML
            [1] => PHP
        )

)*/
?>

<hr>

<?php
//Multiple arrays
$a=array("a"=>array("A"),"b"=>array("Java","C+"));
$b=array("a"=>array("B"),"b"=>array("Python"));
$c=array("a"=>array("C"),"b"=>array("PHP"));
echo '<pre>';
print_r(array_replace_recursive($a,$b,$c));

echo '</pre>';
/*Array
(
    [a] => Array
        (
            [0] => C
        )

    [b] => Array
        (
            [0] => PHP
            [1] => C+
        )

)*/

?>

<hr>


<?php//from j  av a 2 s  . com
    $a1=array("a"=>array("A"),"b"=>array("Java","PHP"),);
    $a2=array("a"=>array("B"),"b"=>array("HTML"));
    
    $result=array_replace_recursive($a1,$a2);
   
    print_r($result);
  
    
    $result=array_replace($a1,$a2);
  
    print_r($result);
    
?>