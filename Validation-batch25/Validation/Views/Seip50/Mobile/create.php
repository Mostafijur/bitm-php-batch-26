<?php
session_start();
?>
<html>
<head>
    <title>Favorite Mobile Models</title>
</head>
<body>
<fieldset>
    <legend>Add favorite Mobile Models</legend>
    <form action="store.php" method="post">
        <?php
        if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
            echo $_SESSION['Message'];
            unset($_SESSION['Message']);
        }
        ?>
        <label>
            Enter Your Favorite Mobile Model
        </label>
        <input type="text" name="Mobile_model">
        <input type="submit" value="Add">
    </form>
</fieldset>
</body>
</html>
