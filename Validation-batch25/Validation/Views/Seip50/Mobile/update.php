<?php
include_once("../../../vendor/autoload.php");

use App\Seip50\Mobile\Mobile;
$obj = new Mobile();
//echo $_SESSION['id'];
if(isset($_SESSION['id']) && !empty($_SESSION['id'])){
    $_POST['id'] = $_SESSION['id'];
    $obj->prepare($_POST)->update();
    unset($_SESSION['id']);
}else{
    $_SESSION['Message'] = "Sorry";
    header('location:404.php');
}
