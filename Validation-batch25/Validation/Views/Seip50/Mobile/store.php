<?php
//include_once "../../../Src/Seip50/Mobile/Mobile.php";
include_once("../../../vendor/autoload.php");

use App\Seip50\Mobile\Mobile;

$obj = new Mobile();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $obj->prepare($_POST);
    $obj->store();
} else {
    $_SESSION['Message'] = "Opps Sorry you are not authorized for this page";
    header('location:404.php');
}