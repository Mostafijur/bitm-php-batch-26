<?php

/* 
 * This constant returns the function name as it was declared (case-sensitive) 
 * This interesting PHP Magic Constant gives the function name
 */

//Define function
function sayHello(){
    echo 'Hello by '.__FUNCTION__. " function";
    
}
//function call
sayHello();

?>