<?php

/* 
 * The current line number of the file.
 * It is a constant of dynamic nature as it depends on the line that it’s used on in your script. 
 * It is very useful constant for debugging or any other purpose. 
 */

echo __LINE__;

?>