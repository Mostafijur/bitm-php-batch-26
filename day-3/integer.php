<?php

/* 
 * Data Types in PHP
 *Integers are whole numbers, without a decimal point (..., -2, -1, 0, 1, 2, ...).
 */

$a=123;    // decimal number
var_dump($a);
echo '<br>'; 

$b=-123;  // a negative number
var_dump($a);
echo '<br>';

$c=0x1A;  // hexadecimal number
var_dump($c);
echo '<br>';

$d=0123; // octal number
var_dump($d);

?>
