<?php

/* 
 * Data Types in PHP
 * Floating point numbers (also known as "floats", "doubles", or "real numbers") 
 * are decimal or fractional numbers
 */

$a=1.2345;
var_dump($a);
echo '<br/>';

$b=10.2e3;
var_dump($b);
echo '<br/>';

$c = 4E-10; 
var_dump($c);
?>