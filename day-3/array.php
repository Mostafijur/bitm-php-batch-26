<?php

/* 
 * Data Types in PHP
 *An array is a variable that can hold more than one value at a time.
 */

$colors=array("red","green","blue");
var_dump($colors);
echo '<br/>';

$colors_code=array(
   "red"=>"#ff0000",
   "green"=>"#00ff00",
   "blue"=>"#0000ff"
     
   );
   echo '<pre>';
   print_r($colors_code);
   
   
   ?>