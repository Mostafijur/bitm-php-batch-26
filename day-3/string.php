<?php

/* 
 * Data Types in PHP
 * Strings are sequences of characters, where every character is the same as a byte.
 * A string can hold letters, numbers, and special characters and it can be as 
 * large as up to 2GB (2147483647 bytes maximum). 
 */

$a="Mostafijur";
$b="$a";
$c='@';
echo $a;
echo "<br>";
var_dump($a);
echo "<br>";

echo $b;
echo "<br>";
echo $c;
?>