<?php

/* 
 * The foreach Loop is used to display the value of array.

You can define two parameter inside foreach separated through “as” keyword.
First parameter must be existing array name which elements or key you want to display.

At the Position of 2nd parameter, could define two variable: One for key(index)
and another for value.

if you define only one variable at the position of 2nd parameter it contain arrays value (By default display array value).
 * foreach ($array as $value)
  	{
	code to be executed;
  	} 
 * foreach($array as $key => $value){
    // Code to be executed
} 
 */

$array=array("Red","black","Green","Blue");

foreach($array as $value){
    
//    echo '<pre>';
//    print_r($value);
  echo $value. "<br>";
}
/*Red
black
Green
Blue*/

?>


<hr>

<?php
$name=array("Name"=>"Muaz",
            "Age"=>1,
            "Email"=>"muaz@gmail.com");

foreach($name as $key=>$address){
    
    echo $key.': '.$address." <br/>";
    
}
/*Name: Muaz
Age: 1
Email: muaz@gmail.com */

?>


<hr>

<?php
//Find the Sum of given array 

$number=array(10,20,30,40);
$sum=0;

foreach ($number as $x){
    
    $sum=$sum+$x;
}
   echo 'Sum : '.$sum;
   
   //Sum : 100
?>