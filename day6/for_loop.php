<?php

/* 
 * The for loop repeats a block of code until a certain condition is met. 
 * It is typically used to execute a block of code for certain number of times.
 * for(initialization; condition; increment){
    // Code to be executed
} 
 */
//print the statement 5 times
for($i=1;$i<=5;$i++){
    
    echo 'The number is : '.$i."<br>";
}

  /*The number is : 1
The number is : 2
The number is : 3
The number is : 4
The number is : 5*/   

?>

<hr>

<?php
//Write a program to print your name 10 times
$name="Muaz";
for($i=1;$i<=10;$i++){
    echo 'My name ia: '.$name."<br>";
}
  /*My name ia: Muaz
My name ia: Muaz
My name ia: Muaz
My name ia: Muaz
My name ia: Muaz
My name ia: Muaz
My name ia: Muaz
My name ia: Muaz
My name ia: Muaz
My name ia: Muaz*/   
?>

<hr>

<?php
//Find the sum of 1 to 100.
$sum=0;
for($i=1;$i<=100;$i++){
    
    $sum=$sum+$i;
}
   echo $sum;

   
     /*5050 */   

?>

<hr>

<?php
//Find all even numbers between 1 to 100 
for($i=2;$i<=100;$i+=2){
    
    echo $i.' ,';
    
      /*2 ,4 ,6 ,8 ,10 ,12 ,14 ,16 ,18 ,20 ,22 ,24 ,26 ,28 ,30 ,32 ,34 ,36 ,38 ,40 ,42 ,44 ,46 ,48 ,50 
      ,52 ,54 ,56 ,58 ,60 ,62 ,64 ,66 ,68 ,70 ,72 ,74 ,76 ,78 ,80 ,82 ,84 ,86 ,88 ,90 ,92 ,94 ,96 ,98 ,100 , */   

}



?>



<hr>

<?php

//Find all even numbers between 3 to 12 

for($i=4;$i<=12;$i+=2){
    
    echo $i.' ,';
    
} 

  /*4 ,6 ,8 ,10 ,12 ,*/   

?>

<hr>

<?php

//Find all odd numbers between 1 to 100 using loop.

for($i=1;$i<=100;$i+=2){
    
    echo $i.' ,';
    
} 

  /*1 ,3 ,5 ,7 ,9 ,11 ,13 ,15 ,17 ,19 ,21 ,23 ,25 ,27 ,29 ,31 ,33 ,35 ,37 ,39 ,41 ,43 ,45 ,47 ,49 ,
   51 ,53 ,55 ,57 ,59 ,61 ,63 ,65 ,67 ,69 ,71 ,73 ,75 ,77 ,79 ,81 ,83 ,85 ,87 ,89 ,91 ,93 ,95 ,97 ,99 ,,*/   

?>