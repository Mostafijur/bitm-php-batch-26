<?php

/* 
 * The if statement is used to execute a block of code only if the specified condition evaluates to true. 
 * if(condition){
    // Code to be executed
} 
 */

$day=date('D');
if($day=='Tue'){
    echo 'Have a nice day!';
}

?>


