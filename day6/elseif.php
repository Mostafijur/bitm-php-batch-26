<?php

/* 
 * The if...elseif...else a special statement that is used to combine multiple if...else statements.
if(condition){
    // Code to be executed if condition is true
} elseif(condition){
    // Code to be executed if condition is true
} else{
    // Code to be executed if condition is false
}

 */

$d=  date('D');
if($d=='Fri'){ 
    echo 'Have a nice weekend';
}elseif ($d=='Mon') {
    echo 'Have a nice wednesday';
}  else {
    echo 'Have a nice day';    
}

?>