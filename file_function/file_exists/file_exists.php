<?php
/*

The file_exists() function checks whether or not a file or directory exists.
Syntax
file_exists(path) 

*/

$file="test.txt";

$file2="google";

echo file_exists($file)."<br>";
echo file_exists($file2);

/*
  1 
 
  1
  */

?>

<hr>

<?php
$file="test.txt";

if(file_exists($file)){

	echo file_get_contents($file);
}




?>