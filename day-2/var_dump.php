<?php

/* 
 The var_dump() function is used to display structured information (type and value) 
 * about one or more variables.
 */

$a=array(123,'hello',12.34);
var_dump($a);
?>
<hr>
<?php

$a="hello world";
$b=12345;
$c=5466.87;
var_dump($a).'<br>';
var_dump($b).'<br>';
var_dump($c).'<br>';



?>