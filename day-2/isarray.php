<?php

/* 
 The is_array() function is used to find whether a variable is an array or not.
 */

$var_name=array('A','B','C');
if(is_array($var_name)){
    echo 'This is an array....';
}  else {
    echo 'This is not array....'; 
}


?>