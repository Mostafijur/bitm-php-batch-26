<?php
/* 
 * The empty() function is used to check whether a variable is empty or not
 * Returns FALSE if var exists and has a non-empty, non-zero value. Otherwise returns TRUE.

The following things are considered to be empty:

    "" (an empty string)
    0 (0 as an integer)
    0.0 (0 as a float)
    "0" (0 as a string)
    NULL
    FALSE
    array() (an empty array)
    $var; (a variable declared, but without a value)*/

$a=0;
$b=NULL;
$c='Learning empty';

if(empty($a)){
    echo '$a' . "  is empty or 0";//$a is empty or 0
}  else {
    echo '$a' . "  is not empty or 0";    
}
   echo '<br/>';
if(empty($b)){
    echo '$b'."  is empty or 0";//$b is empty or 0
}  else {
   echo '$b'."   is not empty or 0"; 
}

  echo '<br/>';
if(empty($c)){
    echo '$c'."  is empty or 0";
    
}  else {
    echo '$c'."  is not empty or 0";//$c is not empty or 0
}

?>