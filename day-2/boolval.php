<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
      echo '0:    '.(boolval(0)? 'true':'false')."<br>"; //integer – 0 is false, everything else is true
      echo '42:   '.(boolval(42)? 'true':'false')."<br>";
      echo '0.0:   '.(boolval(0.0)?'true':'false')."<br>";//float – 0.0 is false, everything else is true
      echo '2.2 :  '.(boolval(2.2)?'true':'false')."<br>";
      echo '"":    '.(boolval("")? 'true':'false')."<br>";//null string are false, everything else is true
      echo '"string":'.(boolval("string")?'true':'false')."<br>";
      echo '"0"  : '.(boolval("1")?'true':'false')."<br>";
      echo '[1, 2] '.(boolval([1, 2])? 'true':'false')."<br>";
      echo '[ ] :   '.(boolval([])?'true':'false')."<br>";//array – empty array is false, everything else is true
      echo 'new stdClass : '.(boolval(new stdClass) ? 'true' : 'false')."<br>";//object
      echo 'Null :    '.(boolval(Null)?'true':'false')."<br>";//null – null is false
      ?>
    </body>
</html>
