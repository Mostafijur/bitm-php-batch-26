<?php

/* 
 In case you were still curious about the "secondary output",
 *  its fairly simple: a = array, 3 = of size three elements within the {}'s. 
 * inside that, you have i=integer/index equalling 1, 
 * string of len 6 equalling "elem 1", integer equalling 2.. etc etc.. 
 */

$serialize_data=array("Math","English","Stat");
//var_dump(serialize($serialize_data));
print_r(serialize($serialize_data));
//a:3:{i:0;s:4:"Math";i:1;s:7:"English";i:2;s:4:"Stat";}
?>